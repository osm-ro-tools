#!/bin/sh

APP="$0"

usage_pa ()
{
	cat <<EOHELP

  Utilizare: $APP -i map.osm -p dir_poly -d dir_fin
  Utilizare: $APP -i map.pbf -p dir_poly -d dir_fin

   -i map.[osm|pbf]   extrage hărți din harta-sursă dată ca parametru
   -p dir_poly  dir_poly este directorul unde se caută fișierele poly
   -d dir_fin   dir_fin este directorul destinație pentru hărțile extrase

EOHELP

	exit $1
}

[ $# -lt 6 ] && usage_pa 1

while [ $# -gt 1 ] ; do
	case $1 in
		-i) shift; MAPIN="$1"; shift;;
		-p) shift; POLYD="$1"; shift;;
		-d) shift; OUTD="$1"; shift;;
		*) echo "Parametru neașteptat..."; usage_pa 1 ;;
	esac
done

[ "$MAPIN" ] || usage_pa 2
[ "$OUTD" ]  || usage_pa 2
[ "$POLYD" ] || usage_pa 2

[ -d "$OUTD" ] || mkdir -p "$OUTD"

for jp in $POLYD/*.poly; do
	jud="$(basename ${jp%.poly})"
	suf="${MAPIN##*.}"
	echo "extract:$jud"
	osmconvert "$MAPIN" -B="$jp" -o="$OUTD/$jud.$suf"
done

