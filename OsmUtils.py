#-*- coding: utf-8 -*-
# vim:noet:ts=4:sw=4:

# Various OSM-related utilities useful for mass manipulation

__version__ = '0.1'

from OsmApi import OsmApi
from math import cos, radians
import re
import os
from copy import deepcopy


class OsmApiExt(OsmApi):

	def __init__(self,
		passwordfile=None,
		username=None,
		password=None,
		appname='OsmUtils' + __version__,
		comment=u'updated with OsmUtils'
		):

		if username or password:
			raise NotImplementedError(
						'Username+password authentication not implemented')

		if not passwordfile:
			homedir = os.environ['HOME']
			self._pwfile = homedir + '/.config/osm-utils/osm-auth'
		else:
			self._pwfile = passwordfile

		api = OsmApi(passwordfile=self._pwfile, appid=appname,
					changesetauto=True,
					changesetautotags={u'comment': comment})
		self._api = api
		self._map = None

	def __del__(self):
		del self._api
		del self._map
		return None

	def MapAroundPoint(self, lon=None, lat=None, bbox_km=10):
		"""
	Given the latitude 'lat' and longitude 'lon', get from the API the map
	around that point within a bbox_km area

	Returns a list of dict {type: node|way|relation, data: {}}. """

		if not lon and self._lon:
			lon = self._lon
		else:
			raise ValueError

		if not lat and self._lat:
			lat = self._lat
		else:
			raise ValueError

		# one degree latitude is approximately 111km
		# and we want to know what's half of bbox_km in lat degrees
		delta_lat = bbox_km / 222.0

		lat = float(lat)
		lon = float(lon)
		# one degree longitude is a cos(lat) * 111
		# and we want to know what's half of bbox_km in lon degrees
		delta_lon = cos(radians(lat)) * delta_lat

		lat_b = lat - delta_lat
		lat_t = lat + delta_lat

		lon_l = lon - delta_lon
		lon_r = lon + delta_lon

		self._map = self._api.Map(lon_l, lat_b, lon_r, lat_t)
		return self._map

	def mapFocus(self, lat, lon):
		self._lat = lat
		self._lon = lon

	def getApiMap(self, lat=None, lon=None, bbox=None, amap=None):

		if not amap:
			if not lat:
				if not self._lat:
					raise NameError('lat not defined')
				else:
					lat = self._lat
			if not lon:
				if not self._lon:
					raise NameError('lon not defined')
				else:
					lon = self._lon
			self.mapFocus(lat, lon)
			if not bbox:
				amap = self.MapAroundPoint()
			else:
				amap = self.MapAroundPoint(bbox_km=bbox)
		else:
			self._map = deepcopy(amap)

		return self._map

	@staticmethod
	def grepElementsInMap(amap, tag, pattern, etype=u'way', flags=re.I + re.U):

		# obtain all elements of type etype
		elems = [x[u'data'] for x in amap if (x[u'type'] == etype)]

		regex = re.compile(pattern, flags)
		result = []
		for x in elems:
			try:
				if regex.match(x[u'tag'][tag]):
					result.append(x)
			except KeyError:
				pass

		if not result:
			return []
		else:
			return result
