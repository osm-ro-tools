#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import unicodedata
from xml.dom import minidom

import os
if os.path.exists('OsmApi.py'):
	import sys
	sys.path.insert(0,'.')
from OsmApi import OsmApi

from math import cos, radians

if os.path.exists('siruta.py'):
	if not '.' in sys.path:
		sys.path.insert(0,'.')
from sirutacsv import SirutaDictReader

import getopt


def stripAccents(s):
	"""strip any accents that may exist in a unicode object and leave only the base ASCII char"""

	try:
		s = unicode(s,'utf-8')
	except TypeError:
		pass
	s = s.replace(u"-", u" ")
	return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


def simplifyName(unicode_name):
	"""a function to turn into lowercase ASCII any name (by stripping accents)"""

	simplename = unicode_name.lower()
	simplename = stripAccents(simplename)

	return simplename

def getNodesList(osmxmlcontents):
	"""extracts only the nodes from an OSM XML and stores them in a OsmApi structure"""

	if len(osmxmlcontents) > 1:
		raise ValueError('Too many osm blocks in one XML')

	# select only the nodes from the OSM XML
	xmlnodes = osmxmlcontents[0].getElementsByTagName ('node')

	# TODO: use an internal data member for the api
	api = OsmApi()
	osmnodes = []
	for xmlnode in xmlnodes:
		osmnode = api._DomParseNode(xmlnode)
		osmnodes.append(osmnode)

	return osmnodes


def getMatchingPlaces(osmapielements,placename, sirutacode = None):
	"""filters from 'osmapielements' only the places with the same simplified name as 'placename'"""
	list = []

	# TODO: try somehow to match î with â when is correct to do so
	#       should also match i to â (when is correct)
	# TODO: maybe calculating the lexical distance is the way to fix this
	#       and match also typo-ed names with correct ones
	sname = simplifyName (placename)

	for i in osmapielements:
		try:
			if len( i[u"tag"][u"place"] ) > 0 and \
				sname == simplifyName(i[u"tag"][u"name"]):
				list.append(i)
		except KeyError:
			# that node didn't have the 'place' or a 'name' tag, so is uninteresting
			pass

	# try to see if there's already a siruta code attached
	# if it is, filter by it, and if the result is non-empty, return that list
	if len(list) > 1 and sirutacode:
		samesiruta = getSameSiruta(list, sirutacode)
		if len(samesiruta) != 0:
			list = samesiruta
	return list


def locatePlaceInXML(xml,placename,sirutacode=None):
	"""Looks for nodes which have the attribute 'place' and whose name looks like 'placename'
into the xml document 'xml'. The input xml document is the one returned by the OSM API
through a query for data within a bounding box"""

	if os.path.exists(xml):
		xmldoc = minidom.parse(xml)
	else:
		xmldoc = minidom.parseString(xml.encode("utf-8"))

	# each OSM XML has a single root osm element
	osmxmlcontents = xmldoc.getElementsByTagName('osm')

	nodeslist = getNodesList(osmxmlcontents)

	nodeswithtags = [ x for x in nodeslist if len(x[u"tag"]) > 0 ]

	places = getMatchingPlaces(nodeswithtags, placename, sirutacode)

	return places

def getArea ( bbox_str ):
	"""Given the bounding box defined by the bbox_str string, return the map within that bbox"""

	path = "/api/0.6/map?bbox=" + bbox_str

	# TODO: use an internal data member for the api
	api = OsmApi()
	data = api._get ( path )

	return data

def getMapAroundPoint(lon, lat, bbox_km = 10):
	"""Given the latitude 'lat' and longitude 'lon', get from the API the map around that point
within a bbox_km area"""

	# one degree latitude is approximately 111km
	# and we want to know what's half of bbox_km in lat degrees
	delta_lat = bbox_km / 222.0

	lat = float (lat)
	lon = float (lon)
	# one degree longitude is a cos(lat) * 111
	# and we want to know what's half of bbox_km in lon degrees
	delta_lon = cos( radians (lat) ) * delta_lat

	lat_b = lat - delta_lat
	lat_t = lat + delta_lat

	lon_l = lon - delta_lon
	lon_r = lon + delta_lon


	path = "%.6f,%.6f,%.6f,%.6f" % ( lon_l, lat_b, lon_r, lat_t )

	area_xml_string = getArea ( path )

	return area_xml_string

def simpleName(placename):
	"""Removes from a name of a place any prefix that indicates its clasification"""
	simpleplacename = placename.replace(u"Municipiul ",u"",1)
	simpleplacename = simpleplacename.replace(u"Oraș ",u"",1)

	return simpleplacename

def sirutaTypeToPlace(sirutarank, population):
	"""Maps siruta ranks to proper 'place' values. The siruta types are explained bellow

Cod	Denumire tip de unitate administrativ teritorială

 40	Judeţ, municipiul Bucureşti
  1	Municipiu  reşedinţă de judeţ, reşedinţă a municipiului Bucureşti
  2	Oraş ce aparţine de judeţ, altul decât oraş  reşedinţă de judeţ
  3	Comună
  4	Municipiu, altul decât reşedinţă de judeţ
  5	Oraş reşedinţă de judeţ
  6	Sector al  municipiului Bucureşti
  9	Localitate  componentă,  reşedinţă de  municipiu
 10	Localitate componentă, a unui municipiu alta decât reşedinţă de municipiu
 11	Sat ce aparţine de municipiu
 17	Localitate componentă reşedinţă a oraşului
 18	Localitate  componentă a unui oraş, alta decât reşedinţă de  oraş
 19	Sat care aparţine  unui oraş
 22	Sat reşedinţă de comună
 23	Sat ce aparţine de comună, altul  decât reşedinţă de comună
"""

	rank = int(sirutarank)

	# municipii, reședințe de județ, reședințe de municipiu
	if rank in [ 1, 4, 9, 40 ]:
		return u"city"
	# orașe, orașe reședință de județ, reședințe ale orașelor
	if rank in [ 2, 5, 17 ]:
		return u"town"
	# localități componente ale orașelor sau municpiilor, altele decât reședințele
	if rank in [ 10, 18 ]:
		return u"village"
	# comune, sate parte din municipii, sate parte din orașe, reședințe de comună, sate non-reședință
	if rank in [ 3, 11, 19, 22, 23 ]:
		# doar satele non-reședință ce aparțin de comune pot fi cătune (hamlet)
		if rank == 23 and int(population) < 50:
			return u"hamlet"
		else:
			return u"village"
	# sectoarele municipiului București
	if rank in [ 6 ]:
		return u"sector"

	raise ValueError, "Unexpected rank value in siruta data"

def nodeDictForPlace(sirutadict, oldnode = None):
	"""Creates a proper dictionary structure for the node defined in 'sirutadict' taking into account
the existing data which is present in 'oldnode'"""

	node = {}
	tags = {}

	# it seems some of the input contains no data for the population and the rank,
	# which probably means 0 for population, and we don't care for rank

	if sirutadict[u"population2002"] == u"":
		sirutadict[u"population2002"] = u"0"

	if oldnode:
		node = oldnode
		tags = oldnode[u"tag"]

		if not u"population" in tags:
			tags[u"population"] = sirutadict[u"population2002"]

		if u"postal_code" in tags:
			if int(tags[u"postal_code"]) == int(sirutadict[u"old_postal_code"]):
				tags.pop(u"postal_code")

		if u"addr:postcode" in tags:
			if int(tags[u"addr:postcode"]) == int(sirutadict[u"old_postal_code"]):
				tags.pop(u"addr:postcode")


	else:
		node[u"lat"] = float(sirutadict[u"lat"])
		node[u"lon"] = float(sirutadict[u"lon"])
		tags[u"population"] = sirutadict[u"population2002"]

	# consistently add the 1992 census data
	tags[u"population:census:1992"] = sirutadict[u"population2002"]

	# this should probably be ran even for existing nodes
	tags[u"place"] = sirutaTypeToPlace(sirutadict[u"siruta:type"], tags[u"population"])

	# clean up siruta:name_sup
	sirutadict[u"siruta:name_sup"] = simpleName(sirutadict[u"siruta:name_sup"])

	uninteresting = [ \
				u"lon", \
				u"lat", \
				u'siruta:rank', \
				u"population2002", \
				u"region", \
				u"siruta:region_id", \
				u"siruta:enviro_type", \
				u"siruta:sortcode" ]

	mergetags = sirutadict.copy()
	for tag in sirutadict:
		if tag in uninteresting:
			mergetags.pop(tag)

	tags.update(mergetags)

	simplesup = simpleName(sirutadict[u"siruta:name_sup"])
	is_in = [u"România"]
	tags[u"is_in:country"] = u"România"
	is_in.insert(0,sirutadict[u"siruta:county"])
	tags[u"is_in:county"] = sirutadict[u"siruta:county"]
	if tags[u"name"] <> simplesup:
		is_in.insert(0,simplesup)

	tags[u"is_in"] = u";".join(is_in)

	# prune the created_by tag since is deprecated to have it on the
	# node and the changeset contains that info anyway
	if u"created_by" in tags:
		tags.pop(u"created_by")

	node[u"tag"] = tags

	return node

def getSameSiruta(elementlist, sirutacode):
	"""returns a list with all the elements in list which have the siruta:code == sirutacode"""

	newlist = []
	for x in elementlist:
		try:
			if x[u"tag"][u"siruta:code"] == sirutacode:
				newlist.append(x.copy())
		except KeyError:
			pass

	return newlist

def readAndProcessSirutaCsv(file, comment = None, source = None, bbox_km = 10):
	"""reads the input CSV file and processes each entry"""

	csvfile = open (file, 'r')
	reader = SirutaDictReader( csvfile )

	homedir = os.environ['HOME']
	api = OsmApi(passwordfile = homedir + '/.config/osm-import-loc/osm-auth', appid = 'RoOsmLocImporter')

	if not comment:
		comment = 'import places from ' + file
	else:
		comment = unicode(comment,'utf-8')
	if not source:
		source = u"http://geo-spatial.org siruta data import"
	else:
		source = unicode(source,'utf-8')
	cs_tags = { 'comment' : comment , 'source' : source }

	print >> sys.stderr, "New pocessing started..."
	api.ChangesetCreate(cs_tags)

	for csvplace in reader:

		uname = csvplace[u"name"].encode("utf-8")
		print "Processing data for %s ..." % ( uname )
		sys.stdout.flush()
		map = getMapAroundPoint ( lat = csvplace[u"lat"], lon = csvplace[u"lon"], bbox_km = bbox_km )
		existing_nodes = locatePlaceInXML ( map, csvplace[u"name"], csvplace[u"siruta:code"] )

		if len(existing_nodes) == 0:
			# node doesn't exist for this place, or is far; we can create the node
			nodedict = nodeDictForPlace ( csvplace )

			api.NodeCreate(nodedict)
			print "Created new node for %s" % ( uname )

		elif len(existing_nodes) == 1:
			# there is an existing code, so we merge with that
			referencenode = existing_nodes[0].copy()
			# dictionaries don't get copied by default
			referencenode[u"tag"] = existing_nodes[0][u"tag"].copy()
			nodedict = nodeDictForPlace ( csvplace, existing_nodes[0] )

			if nodedict == referencenode:
				print "Skipping: No changes needed for node %s" % ( uname )
			else:
				api.NodeUpdate(nodedict)
				print "Updated existing node for %s" % ( uname )

		else:
			# I am confused, more than one node with the same simplified name

			print >> sys.stderr, "Skipping %s: Too many (%d) existing nodes with the same name at (lat=%s&lon=%s)" % (
					uname,
					len(existing_nodes),
					csvplace[u"lat"].encode("utf-8"),
					csvplace[u"lon"].encode("utf-8") )

		sys.stdout.flush()
		sys.stderr.flush()

	api.ChangesetClose()
	csvfile.close()

def usage():
	print "%s [-c|--comment <comment>] [-s|--source <source>] -i <inputcsv>" % sys.argv[0]

def main(argv = None):

	if argv is None:
		argv = sys.argv

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hi:c:s:b:",
				["help", "input=", "comment=", "source=","bbox="] )
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err)
		usage()
		return 2

	file = None
	comment = None
	source = None
	bbox_km = 10

	for o,a in opts:
		if o in ("-h", "help"):
			usage()
			return 0
		elif o in ("-i", "--input"):
			file = a
		elif o in ("-s", "--source"):
			source = a
		elif o in ("-c", "--comment"):
			comment = a
		elif o in ("-b", "--bbox"):
			bbox_km = float(a)

	if not file:
		print "Input csv file (-i option) is mandatory. Run the script with -h for online help."
		return 2


	readAndProcessSirutaCsv(file, source=source, comment=comment, bbox_km=bbox_km)


if __name__ == "__main__":
	sys.exit(main())
