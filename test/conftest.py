#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 27.10.2012

@author: eddy
'''

import pytest
import sys
sys.path.append('.')

emptymap = []
notagswaymap = [{u'type': u'way', u'data': {u'id': 1, u'tag': {}}}]
notagsnodemap = [{u'type': u'node', u'data': {u'id': 1, u'tag': {}}}]

simplemaps = [emptymap, notagsnodemap, notagswaymap]


# mostly for do_correct_names
nametagwaymap = [{u'type': u'way', u'data': {u'id': 1, u'tag': {u'name':u'Alea Prunului'}}}]
rentagwaymap = {
                u'way': [
                         {u'id': 1, u'tag': {u'name':u'Aleea Prunului'}}
                         ]
                }

# TODO: check why unicodes don't work properly
#sametagwaymap = [{u'type': u'way', u'data': {u'id': 1, u'tag': {u'name': u'Fundătura Prunului'}}}]
sametagwaymap = [{u'type': u'way', u'data': {u'id': 1, u'tag': {u'name': u'Fundacul Prunului'}}}]


strtagwaymap = [{u'type': u'way', u'data': {u'id': 1, u'tag': {u'name':u'St. Prunului'}}}]
rstrtagwaymap = {
                u'way': [
                         {u'id': 1, u'tag': {u'name':u'Strada Prunului'}}
                         ]
                }


# mostly for do_eref2int
oneerefinrefmap = [
                   {u'type': u'way',
                    u'data': {u'id': 1, u'tag': {u'ref':u'e 80'}}
                    }
                   ]
oneerefinrefch =  [
                   {u'type':   u'way',
                    u'data':   {u'id': 1, u'tag': {u'int_ref':u'E80'}},
                    'oldrecs': {u'ref': u'e 80', u'int_ref': u''},
                    'newrecs': {u'ref': u''    , u'int_ref': u'E80'}
                    }
                   ]

duperefinrefmap = [
                   {u'type': u'way',
                    u'data': {u'id': 2, u'tag': {u'ref': u'e 80', u'int_ref':u'E80'}}
                    }
                   ]
duperefinrefch =  [
                   {u'type':   u'way',
                    u'data':   {u'id': 2, u'tag': {u'int_ref':u'E80'}},
                    'oldrecs': {u'ref': u'e 80', u'int_ref': u'E80'},
                    'newrecs': {u'ref': u''    , u'int_ref': u'E80'}
                    }
                   ]


# for do_stdrefs and do_norefsinname
samerefinrefmap = [
                   {u'type': u'way',
                    u'data': {u'id': 1, u'tag': {u'ref':u'DN6', u'name': 'DN6'}}
                    }
                   ]
samenameinrefch =  [
                  {u'type'  : u'way',
                   u'data'  : {u'id': 1, u'tag': {u'ref': u'DN6'}},
                   'oldrecs': {u'ref': u'DN6', u'name': u'DN6'},
                   'newrecs': {u'ref': u'DN6' }
                   }
                  ]


# for do_stdrefs
natrefinrefmap = [
                   {u'type': u'way',
                    u'data': {u'id': 1, u'tag': {u'ref':u'Dn 6', u'name': 'Dn6'}}
                    }
                   ]
natrefinrefch =  [
                  {u'type'  : u'way',
                   u'data'  : {u'id': 1, u'tag': {u'ref': u'DN6', u'name': 'Dn6'}},
                   'oldrecs': {u'ref': u'Dn 6'},
                   'newrecs': {u'ref': u'DN6' }
                   }
                  ]

mixrefinrefmap = [
                   {u'type': u'way',
                    u'data': {u'id': 1, u'tag': {u'ref':u'Dn 6; e70', u'name': 'Dn6'}}
                    }
                   ]
mixrefinrefch = [
                 {u'type':   u'way',
                  u'data':   {u'id': 1, u'tag': {u'ref': u'DN6;E70', u'name': 'Dn6'}},
                  'oldrecs': {u'ref': u'Dn 6; e70'},
                  'newrecs': {u'ref': u'DN6;E70' }
                  }
                 ]

# mostly for get_shiftedpat
shiftpat = [{'in':['One ', 1, u' two ', 2], 'shift':0, 'out': u'One \\1 two \\2'}]

#@pytest.fixture(scope="module")
#def apiprovider():
#    from OsmUtils import OsmApiExt()

#@pytest.fixture(params=simplemaps)
#def pytest_funcarg__mapprovider(pos):
#    return simplemaps[pos]


class fakeApi():
    def __init__(self, exupds, eltype):
        self.eltype = eltype
        self.upds = {eltype: []}
        self.nonempty = False
        self.exupds = exupds
        self.cursors = {u'way': 0, u'node': 0, u'relation': 0}
        if ((len(self.exupds.get(u'way',      [])) > 0) or
            (len(self.exupds.get(u'node',     [])) > 0) or
            (len(self.exupds.get(u'relation', [])) > 0)
            ):
                self.nonempty = True

    def __del__(self):
        self.allExpectedChanges()

    def noChanges(self):
        assert len(self.upds.get(u'way', [])) == 0
        assert len(self.upds.get(u'node', [])) == 0
        assert len(self.upds.get(u'relation', [])) == 0
        return True

    def allExpectedChanges(self):
        if self.nonempty:
            assert self.exupds == self.upds
        else:
            self.noChanges()

    def __elUpdate(self, updatedel, extype):
        assert self.eltype == extype
        ulist = self.upds[self.eltype]
        cexpect = self.exupds[self.eltype][self.cursors[extype]]
        exid = cexpect[u'id']
        id = updatedel[u'id']
        if (id != exid):
            raise TypeError, u"Expected element with id=%d , but found something else" % exid
        self.cursors[extype] += 1;
        ulist.append(updatedel)
        assert len(ulist) == self.cursors[extype]

    def WayUpdate(self, updatednode):
        self.__elUpdate(updatednode, u'way')

    def NodeUpdate(self, updatednode):
        self.__elUpdate(updatednode, u'node')
