import pytest
from conftest import *
from OsmUtils import OsmApiExt
from copy import deepcopy


def test_grepall():
    testmap = deepcopy(nametagwaymap)
    l = OsmApiExt.grepElementsInMap(testmap, u'name', u'.')
    assert len(l) == len(nametagwaymap)


#def test_OsmApiExt():
#    assert 0
