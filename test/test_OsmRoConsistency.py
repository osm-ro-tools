'''
Tests for test_OsmRoConsistency

@author: eddy
'''

import pytest
from conftest import *


@pytest.mark.parametrize('amap', simplemaps)
def test_OsmRoConsistency(amap):
    from OsmRoConsistency import do_stdref
    newmap = do_stdref(amap)
    assert len(newmap) == 0


@pytest.mark.parametrize(('istr','sep','olist'),
                         [
                          (u'a;a;;',                ';',           [u'a']),
                          (u'a;a;;',               None,           [u'a']),
                          ( 'z;x;y;z;a;b',         None, [u'a', u'b', u'x', u'y', u'z']),
                          (u'b; a; a; c; dd; d;b', u';', [u'a', u'b', u'c', u'd', u'dd'])
                          ])
def test_splitstripunique(istr, sep, olist):
    from OsmRoConsistency import splitstripunique
    if sep:
        r = splitstripunique(istr,sep)
    else:
        r = splitstripunique(istr)
    assert r == olist
    sr = ';'.join(r)
    r = splitstripunique(sr)
    assert r == olist


@pytest.mark.parametrize('amap', simplemaps)
def test_eref2intref_empty(amap):
    from OsmRoConsistency import do_eref2int
    ch = do_eref2int(amap)
    assert len(ch) == 0


@pytest.mark.parametrize(('imap','exch', 'prevch'),
                         [
                         (duperefinrefmap, duperefinrefch, None),
                         (oneerefinrefmap, oneerefinrefch, oneerefinrefch), #TODO: fix this
                         (duperefinrefmap, duperefinrefch, duperefinrefch), #TODO: fix this
                         (duperefinrefmap, duperefinrefch, []),
                         (oneerefinrefmap, oneerefinrefch, None),
                         (oneerefinrefmap, oneerefinrefch, []),
                         ])
def test_eref2intrefmove(imap, exch, prevch):
    if exch == prevch:
        pytest.xfail("on already applied changes 'changes' structures are not properly handled yet")
    from OsmRoConsistency import do_eref2int
    from copy import deepcopy
    mymap = deepcopy(imap)
    if prevch:
        resch = do_eref2int(mymap, prevch)
    else:
        resch = do_eref2int(mymap)
    assert resch == exch


@pytest.mark.parametrize(('imap','exch', 'prevch'),
                         [
                         (samerefinrefmap, [], None),
                         (samerefinrefmap, [], []),
                         (natrefinrefmap, natrefinrefch, None),
                         (natrefinrefmap, natrefinrefch, []),
                         (natrefinrefmap, natrefinrefch, natrefinrefch), #TODO: fix this
                         (mixrefinrefmap, mixrefinrefch, None),
                         (mixrefinrefmap, mixrefinrefch, []),
                         (mixrefinrefmap, mixrefinrefch, mixrefinrefch), #TODO: fix this
                         ])
def test_do_stdref(imap, exch, prevch):
    if exch == prevch and len(exch)>0:
        pytest.xfail("on already applied changes 'changes' structures are not properly handled yet")
    from OsmRoConsistency import do_stdref
    from copy import deepcopy
    mymap = deepcopy(imap)
    if prevch:
        resch = do_stdref(mymap, prevch)
    else:
        resch = do_stdref(mymap)
    assert resch == exch


def test_get_shiftedpatrep():
    from OsmRoConsistency import get_shiftedpatrep
    for s in shiftpat:
        sh = s['shift']
        ilst = s['in']
        out = s['out']
        fout = get_shiftedpatrep(ilst, sh)
        assert fout == out


def test_typerror_get_shiftedpatrep():
    from OsmRoConsistency import get_shiftedpatrep
    with pytest.raises(TypeError):
        get_shiftedpatrep([{}, 0], 0)


def test_usage(monkeypatch, capfd):
    expected = 'Test_command'
    monkeypatch.setattr(sys, 'argv', [expected])
    from OsmRoConsistency import usage, __usage__
    usage()
    resout, reserr = capfd.readouterr()
    assert resout == str((__usage__ + u'\n') % expected).decode()


@pytest.mark.parametrize(('imap', 'exupds', 'eltype'),
                         zip(simplemaps, [{u'way': []}] * len(simplemaps), [u'way']* len(simplemaps)) +
                         [
                          (nametagwaymap, rentagwaymap, u'way'),
                          (sametagwaymap, {u'way': []}, u'way'),
                          (strtagwaymap, rstrtagwaymap, u'way')
                          ]
                         )
def test_do_correct_names(imap, exupds, eltype):
    from OsmRoConsistency import do_correct_names
    api = fakeApi(exupds, eltype)
    do_correct_names(imap, api, True)
    api.allExpectedChanges()


def test__applyChanages2Map():
    from OsmRoConsistency import _applyChanges2Map
    with pytest.raises(NotImplementedError):
        _applyChanges2Map([], None)
