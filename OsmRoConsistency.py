#!/usr/bin/python
# -*- coding: utf-8 -*-

# vim:noet:ts=4:sw=4:

import re
import unicodedata


import os
if os.path.exists('OsmApi.py'):
	import sys
	sys.path.insert(0, '.')
#from OsmApi import OsmApi

from OsmUtils import OsmApiExt

from getopt import getopt
from copy import deepcopy


import logging
logging.basicConfig(filename='area.log', level=logging.INFO)

__stdref_comm__ = \
	u'make refs conform to format En+/D[NJC]n+x?, where n = 0-9 and x = A-Z'
__euref2int_ref_comm__ = u'move EU refs (En+) to int_ref;' + \
			u'Romanian mappers agreed ref should contain only national refs'

__usage__ = "\nUsage:\n\t%s --lat=nn.nnnn --lon=nn.nnnn [--bbox=nn]\n"

patterns = {
		'natref'   : { 're' : u'(d[njc])\s*(\d+)\s*([a-z]?)\s*', 'rpl' : [ 1, 2, 3 ] },
		'intref'   : { 're' : u'(e)\s*(\d+)\s*'                , 'rpl' : [ 1, 2 ]   },

		'strada'   : { 're' : u'^\s*(str?a?|[ds]trada)[.\t ]+(.*)$'        , 'rpl' : [ u'Strada ', 2 ]     },
		'stradela' : { 're' : u'^\s*(str-la|sdla|[ds]tradela)[.\t ]+(.*)$' , 'rpl' : [ u'Stradela ', 2 ]   },
		'alee'     : { 're' : u'^\s*(ale?|alee?a)[.\t ]+(.*)$'             , 'rpl' : [ u'Aleea ', 2 ]      },

		'intrare'  : { 're' : u'^\s*(intr?|intrarea)[.\t ]+(.*)$'          , 'rpl' : [ u'Intrarea ', 2 ]   },
		'fundatura': { 're' : u'^\s*(fndt?|fund[aă]tura)[.\t ]+(.*)$'      , 'rpl' : [ u'Fundătura ', 2 ]  },
		'fundac'   : { 're' : u'^\s*(fdc|fundacul)[.\t ]+(.*)$'            , 'rpl' : [ u'Fundacul ', 2 ]   },

		# maybe 'sp' should be disabled 'cause it might be 'Spitalul'?
		'splai'    : { 're' : u'^\s*(spl?|splaiu?l?)[.\t ]+(.*)$'          , 'rpl' : [ u'Splaiul ', 2 ]    },

		'bulevard' : { 're' : u'^\s*(b[dl]|b[-.]?d?ul|blvd?|bulev|bulevardu?l?)[.\t ]+(.*)$' , 'rpl' : [ u'Bulevardul ', 2 ] },

		'piata'    : { 're' : u'^\s*(pia[tţț][aă]|p-?[tţț]a)[.\t ]+(.*)$'  , 'rpl' : [ u'Piața ', 2 ]      },
		'varf'     : { 're' : u'^\s*(vf|v[iaîâ]rfu?l?)[.\t ]+(.*)$'        , 'rpl' : [ u'Vârful ', 2 ]     },
		'munte'    : { 're' : u'^\s*(mn?t|m-te|m-tele|muntele)[.\t ]+(.*)$', 'rpl' : [ u'Muntele ', 2 ]    }
		}

ref_patterns  = ['natref', 'intref']
name_patterns = ['strada', 'stradela', 'alee',
				'intrare', 'fundatura', 'fundac',
				'splai', 'bulevard',
				'piata',
				'varf', 'munte']


def get_shiftedpatrep(patlist, shift=0):
	"""
Based on the pattern (list) patlist, the numbers representing positions of back
references in the initial meta-regex patlist will be 'shifted' with shift
positions and the resulting real replace pattern will be returned"""
	sl = []

	for e in patlist:
		se = u''
		if type(e) == type(0):
			se = u'\\%s' % (e + shift)
		elif type(e) == type(u''):
			se = e
		elif type(e) == type(''):
			se = e.decode()
		else:
			raise TypeError

		sl.append(se)

	return ''.join(sl)


def do_stdref(amap, changes=[]):

	ch = deepcopy(changes)

	natref = patterns['natref']
	intref = patterns['intref']
	pat = u'(' + natref['re'] + u'\s*|' + intref['re'] + u'\s*)'
	patrep    = get_shiftedpatrep(natref['rpl'], 1)
	patrepalt = get_shiftedpatrep(intref['rpl'], 1 + len(natref['rpl']))

	refedways = OsmApiExt.grepElementsInMap(amap,
										etype=u'way', tag=u'ref', pattern=pat)

	for w in refedways:

		wid = w[u'id']
		in_ref = w[u'tag'][u'ref']
		logging.debug(u'Testing way %d: ref = > %s < ...' % (wid, in_ref))

		refs = [x.strip() for x in in_ref.split(u';')]
		dnref = {}

		for oref in refs:

			try:
				nref = re.sub(pat, patrep, oref, flags=re.I).encode().upper()
			except:
				logging.debug(' >>> alt for >%s<' % (oref))
				nref = re.sub(pat, patrepalt, oref, flags=re.I).encode().upper()

			if len(nref.strip()) > 0:
				dnref[nref] = 1

		lnref = dnref.keys()
		lnref.sort()  # this puts the european refs at the end and adds consistency
		out_ref = u';'.join(lnref)

		if in_ref == out_ref:
			logging.debug(u'     Way %d : ignored (no change): %s -> %s' %
								(wid, in_ref, out_ref))
		else:
			logging.debug(u' >>> Way %d : ref change: %s -> %s' %
								(wid, in_ref, out_ref))
			logging.info('Way %d: ref change: %s -> %s' % (wid, in_ref, out_ref))

			w[u'tag'][u'ref'] = out_ref
			#print(w)
			logging.debug('     api.WayUpdate(w) - w[u\'id\'] = %d' % (wid))
			ch.append({u'type':u'way',
						#u'id': wid, #TODO: check and see if this is useful
						u'data': w,
						'oldrecs':{u'ref': in_ref},
						'newrecs':{u'ref': out_ref}
						})

	logging.info('Done')
	return ch


def _applyChanges2Map(changes, api):
	#TODO: implement this
	print (u"NOT IMPLEMENTED YET: _applyChanges2Map(changes, api)")
	raise NotImplementedError


def standardRefsMap(amap, commextra=u''):
	comm = __stdref_comm__ + commextra
	api = OsmApiExt(appname=u'OsmRoConsistency', comment=comm)

	changes = do_stdref(amap)
	_applyChanges2Map(changes, api)

	del api


def standardRefsXY(lat, lon, bbox_km=10):
	api = OsmApiExt(appname=u'OsmRoConsistency', comment=__stdref_comm__)

	api.mapFocus(lat=lat, lon=lon)

	mymap = api.MapAroundPoint(bbox_km=bbox_km)

	changes = do_stdref(mymap)
	_applyChanges2Map(changes, api)

	del api


def splitstripunique(value, separator=u';'):
	"""
From an initial set of values separated with the separator symbol,
generate a list of unique sorted stripped unicode values.
The input can be string or unicode.

E.g.:
	For input u'b; a; a; c; dd; d;b' the function will return
	[ u'a', u'b', u'c', u'd', u'dd' ]
"""

	if type(separator) != type(u''):
		separator = separator.decode()

	l = map(unicode.strip, value.split(separator))

	dl = {}
	for k in l:
		if k != u'':
			dl[k] = 1
	l = dl.keys()
	l.sort()

	return l


def do_eref2int (amap, changes=[]):
	"""
	Searches in the 'ref' fields of ways in the input amap for values that look
	like references of European roads e.g. "E80", "E 81" and moves these in the
	'int_ref' field where they belong, after correcting the format
	"""
	ch = deepcopy(changes)

	irpat = patterns['intref']['re']
	intrep = get_shiftedpatrep(patterns['intref']['rpl'])
	compre = re.compile(irpat, re.I + re.U)

	refedways = [x for x in amap if x[u'type'] == u'way' and
						x[u'data'].get(u'tag', {}).get('ref', u'') != u'']

	eways = OsmApiExt.grepElementsInMap(etype=u'way',
										tag=u'ref',
										pattern=(u'^.*' + irpat + '.*$'),
										amap=refedways)

	for w in eways:

		wid = w[u'id']
		oref = w[u'tag'][u'ref']
		loref = map(unicode.upper, splitstripunique(oref))
		oiref = w[u'tag'].get(u'int_ref', u'')
		loirefs = map(unicode.upper, splitstripunique(oiref))

		erefs = filter(lambda x: re.match(compre, x), loref)
		non_erefs = [x for x in loref if not x in erefs]
		non_erefs.sort()
		nref = u';'.join(non_erefs)


		d = {}
		for er in erefs + loirefs:
			if er != u'':
				canonical_er = re.sub(compre, intrep, er).encode()
				d[canonical_er] = 1

		lniref = d.keys()
		lniref.sort()
		niref = u';'.join(lniref)

		w[u'tag'][u'ref'] = nref
		if (len(nref)==0):
			del w[u'tag'][u'ref']
		w[u'tag'][u'int_ref'] = niref
		# no need to test if niref is empty,
		# we don't get here unless there are such items

		logging.info(u"Way %d changed: from (ref=\"%s\", int_ref=\"%s\") to (ref=\"%s\", int_ref=\"%s\")",
				wid, oref, oiref, nref, niref)

		ch.append({u'type': u'way',
						u'data': w,
						'oldrecs':{u'int_ref': oiref, u'ref':oref},
						'newrecs':{u'int_ref': niref, u'ref':nref}
						})
		#api.WayUpdate(w)

	logging.info(u"e ref to int DONE")
	return ch


def do_correct_names(amap, api, with_update=False):

	namedways  = [x[u'data'] for x in amap if ((x[u'type'] == u'way') and
							(x[u'data'].get(u'tag', {}).get(u'name', u'') != u''))]
	namednodes = [x[u'data'] for x in amap if ((x[u'type'] == u'node') and
							(x[u'data'].get(u'tag', {}).get(u'name', u'') != u''))]

	prop = {
		u'node': {'name': u'Node', 'updfunc': api.NodeUpdate},
		u'way' : {'name': u'Way' , 'updfunc': api.WayUpdate }
		}

	for ntype in name_patterns:
		logging.debug("Processing type %s...", ntype)
		pat    = patterns[ntype]['re']
		patrep = get_shiftedpatrep(patterns[ntype]['rpl'])
		logging.debug("Pattern: \"%s\"", patrep)
		compre = re.compile(pat, re.I + re.U)

		rennodes = [x for x in namednodes if (re.match(compre, x[u'tag'][u'name']))]
		renways  = [x for x in namedways  if (re.match(compre, x[u'tag'][u'name']))]
		nodecount = len(rennodes)

		pos = 0
		for e in rennodes + renways:
			pos += 1
			etype = u'node' if pos <= nodecount else u'way'

			oname = e[u'tag'][u'name']
			nname = re.sub(compre, patrep, oname).encode()

			if nname != oname:
				e[u'tag'][u'name'] = nname
				logging.info(u"%s %d changed: from name=\"%s\" to name=\"%s\"",
							prop[etype]['name'], e[u'id'], oname, nname)
				if with_update:
					prop[etype]['updfunc'](e)
			else:
				logging.debug(u"%s %d's name is identical after prepocessing pattern \"%s\"",
							prop[etype]['name'], e[u'id'], patrep)

	logging.info('name correction done')


def usage():
	print __usage__ % (sys.argv[0])

if __name__ == '__main__':

	try:
		opts, args = getopt (sys.argv[1:], '', ["lon=", "lat=", "bbox="])
	except GetoptError, err:
		print str(err)
		usage()
		sys.exit(2)

	if args:
		print "Program received unknown arguments"
		usage()
		sys.exit(3)

	_lat = None
	_lon = None
	_bbox = None
	for o, a in opts:
		if o == '--lon':
			logging.info("lon=%s" % (a))
			_lon = a
		elif o == '--lat':
			logging.info("lat=%s" % (a))
			_lat = a
		elif o == '--bbox':
			logging.info("bbox=%s" % (a))
			_bbox = int(a)

	if not _lat or not _lon:
		print "Insuficient arguments. Both lon and lat must be defined."
		usage()
		sys.exit(1)

	if _bbox:
		standardRefsXY(lon=_lon, lat=_lat, bbox_km=_bbox)
	else:
		standardRefsXY(lon=_lon, lat=_lat)
