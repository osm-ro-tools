#!/uss/bin/python

import csv

class SirutaDictReader(csv.DictReader):
	"""
	A CSV dict reader which maps the CSV field names to proper OSM tag names
	"""

	def _mapCSVNamesToOSMNames(self, csvnames):

		namemap = { 'X':u'lon', \
				'Y':u'lat', \
				'NAME':u'name', \
				'SIRUTA':u'siruta:code', \
				'OLD_POSTAL':u'old_postal_code', \
				'RANG':u'siruta:rank', \
				'TIP':u'siruta:type', \
				'SIRUTA_SUP':u'siruta:code_sup', \
				'NAME_SUP':u'siruta:name_sup', \
				'COUNTY':u'siruta:county', \
				'COUNTY_ID':u'siruta:county_id', \
				'COUNTY_MN':u'siruta:county_code', \
				'POP2002':u'population2002', \
				'REGION':u'region', \
				'REGION_ID':u'siruta:region_id', \
				'ENVIRO_TYP':u'siruta:enviro_type', \
				'SORT_CODE':u'siruta:sortcode' }

		osmnames = []
		for name in csvnames:
			# although this will fail if unknown rows appear, that
			# is the intended behaviour since we don't want to silently
			# add or ignore fields unintentionaly
			osmnames.append(namemap[name])

		return osmnames

	def next(self):
		if self.fieldnames is None:
			row = self.reader.next()
			row = self._mapCSVNamesToOSMNames(row)
			self.fieldnames = row

		d = csv.DictReader.next(self)

		for cell in d:
			try:
				d[cell] = unicode(d[cell], 'utf-8')
			except TypeError:
				pass
		return d


