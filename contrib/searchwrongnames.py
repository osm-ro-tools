#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# Requirements: Imposm (Omniscale)
#               sort (GNU coreutils)
#
# © Michael Häckel, GPLv3
#

from imposm.parser import OSMParser
import codecs, re, subprocess

searchtags = [ 'name', 'alt_name', 'addr:name', 'addr:street', 'description' ]

def formatName(input):
  return re.sub(u'[\r\n\t]', ' ', re.sub(u'^(Strada |Str. |Piata |Piața |B-dul |B-dul. |Bulevardul |Calea |Aleea |Fundătura |Fundatura |Soseaua |Șoseaua |Sos. |Șos. |Gara |Stația )', '', input)) + '\n'

def makeList():
  class NameSearch(object):

    def nodes(self, nodes):
      for osmid, tags, refs in nodes:
        for tag in searchtags:
          if tag in tags:
            f.write('n')
            f.write(str(osmid))
            f.write('\t')
            f.write(formatName(tags[tag]))

    def ways(self, ways):
      for osmid, tags, refs in ways:
        for tag in searchtags:
          if tag in tags:
            f.write('w')
            f.write(str(osmid))
            f.write('\t')
            f.write(formatName(tags[tag]))

    def relations(self, relations):
      for osmid, tags, refs in relations:
        for tag in searchtags:
          if tag in tags:
             f.write('r')
             f.write(str(osmid))
             f.write('\t')
             f.write(formatName(tags[tag]))

  f = codecs.open('names.txt', 'w', 'utf8')
  search = NameSearch()
  p = OSMParser(concurrency=4, nodes_callback=search.nodes, ways_callback=search.ways, relations_callback=search.relations)
  p.parse('romania.osm.pbf')
  f.close();

def toAscii(input):
  return input.replace(u'ș',u's').replace(u'ț',u't') \
  .replace(u'â',u'a').replace(u'ă',u'a').replace(u'î',u'i') \
  .replace(u'Ș',u's').replace(u'Ț',u't').replace(u'Â',u'a') \
  .replace(u'Ă',u'a').replace(u'Î',u'i').lower()

def makeComparison():
  s = codecs.open('names-sorted.txt', 'r', 'utf8')
  d = codecs.open('names-evaluated.txt', 'w', 'utf8')
  p = codecs.open('names-evaluated-plain.txt', 'w', 'utf8')

  lastname = ''
  lastnumbers = ''
  found = False
  for line in s:
    splitted = line.split('\t')
    name = splitted[1]
    if toAscii(name) == toAscii(lastname):
      if name != lastname:
        d.write(lastnumbers + '\n' + lastname)
        p.write(lastname)
        found = True
        lastnumbers = splitted[0]
      else:
        lastnumbers = lastnumbers + ', ' + splitted[0]
    else:
      if found:
        d.write(lastnumbers + '\n' + lastname + '\n')
        p.write(lastname + '\n')
        found = False
      lastnumbers = splitted[0]
    lastname = name
  s.close()
  d.close()
  p.close()


print('Making word list.')
makeList()

print('Sorting.')
subprocess.call(['sort', '-k', '2', '-o', 'names-sorted.txt', 'names.txt'])

print('Comparing.')
makeComparison()

print('Finished.')
