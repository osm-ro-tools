#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# Requirements: Imposm (Omniscale)
#               sort (GNU coreutils)
#
# This script searches for the characters Ş, ş, Ţ, ţ
#
# © Michael Häckel, GPLv3
#

from imposm.parser import OSMParser
import codecs, re, subprocess
 
lists = [ [ u'name', []],
	  [ u'name:ro', []],
	  [ u'alt_name', []],
	  [ u'old_name', []],
          [ u'addr:street', []],
          [ u'addr:city', []],
          [ u'addr:housename', []],
          [ u'is_in', []],
          [ u'is_in:city', []],
          [ u'is_in:county', []],
          [ u'is_in:country', []],
	  [ u'description', []],
	  [ u'note', []] ]

def formatName(input):
  return re.sub(u'[\r\n\t]', ' ', input)

def makeList():
  class NameSearch(object):

    def nodes(self, nodes):
      for osmid, tags, refs in nodes:
	for tag, nameList in lists:
          if tag in tags and re.search(u'[ŞşŢţ]', tags[tag]):
              nameList.append(['n', osmid, formatName(tags[tag])])

    def ways(self, ways):
      for osmid, tags, refs in ways:
	for tag, nameList in lists:
          if tag in tags and re.search(u'[ŞşŢţ]', tags[tag]):
            nameList.append(['w', osmid, formatName(tags[tag])])

    def relations(self, relations):
      for osmid, tags, refs in relations:
	for tag, nameList in lists:
          if tag in tags and re.search(u'[ŞşŢţ]', tags[tag]):
            nameList.append(['r', osmid, formatName(tags[tag])])

  search = NameSearch()
  p = OSMParser(concurrency=4, nodes_callback=search.nodes, ways_callback=search.ways, relations_callback=search.relations)
  p.parse('romania.osm')

def writeList():
  d = codecs.open('characters.txt', 'w', 'utf8')
  for tag, nameList in lists:
    nameList.sort(key=lambda name: name[2])
    first = True
    for item in nameList:
      if first:
        d.write(tag + '\n')
        first = False
      else:
        d.write(', ')
      d.write(item[0] + str(item[1]))
    d.write('\n')
    for item in nameList:
      d.write(item[2] + '\n')
    if not first:
      d.write('\n\n')
  d.close()

print('Making list.')
makeList()

print('Writing list.')
writeList()

print('Finished.')
