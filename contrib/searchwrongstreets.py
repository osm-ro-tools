#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# Requirements: Imposm (Omniscale)
#
# This script sorts the highways according to the prefix/postfix in the name.
# It helps finding errors like "Strada Drumul" and undesired forms of the
# various street types. It workes for the streets in Romania and in Moldova
#
# Please note that there are different naming styles in these two countries.
# The extracts from Geofabrik are not a suitable source for the data because
# they use a simplified polygon for cutting. You need the exact border.
#
# © Michael Häckel, GPLv3
#

from imposm.parser import OSMParser
import codecs, re, subprocess
 
searchtags = [ 'name', 'name:ro', 'alt_name', 'addr:name', 'addr:street', 'description' ]


lists = [ [u'acces',             u'^[Aa]cces ', []],
          [u'gass',              u'gass$', []],
          [u'strass',            u'strass$', []],
          [u'Alee',              u'^Alee ', []],
          [u'Aleea',             u'^Aleea ', []],
          [u'Autostrada',        u'^Autostrada ', []],
          [u'Bd.',               u'^Bd. ', []],
          [u'bd.',               u'^bd. ', []],
          [u'Bulevardul',        u'^Bulevardul ', []],
          [u'Calea',             u'^Calea ', []],
          [u'Centura',           u'^Centura ', []],
          [u'Drum',              u'^Drum ', []],
          [u'drum',              u'^drum ', []],
          [u'Drumul',            u'^Drumul ', []],
          [u'Fundacul',          u'^Fundacul ', []],
          [u'Fundătura',         u'^Fundătura ', []],
          [u'Intrare',           u'^Intrare ', []],
          [u'Intrarea',          u'^Intrarea ', []],
          [u'Pasaj',             u'^Pasaj ', []],
          [u'Pasajul',           u'^Pasajul ', []],
          [u'pas.',              u'^pas. ', []],
          [u'Piata',             u'^Piata ', []],
          [u'Piața',             u'^Piața ', []],
          [u'Piaţa',             u'^Piaţa ', []],
          [u'Pod',               u'^Pod ', []],
          [u'pod',               u'^pod ', []],
          [u'Podul',             u'^Podul ', []],
          [u'Prelungirea',       u'^Prelungirea ', []],
          [u'Șos',               u'^[SȘŞ]os ', []],
          [u'Șos.',              u'^Șos. ', []],
          [u'șos.',              u'^șos. ', []],
          [u'Soseaua',           u'^Soseaua ', []],
          [u'Șoseaua',           u'^Șoseaua ', []],
          [u'Şoseaua',           u'^Şoseaua ', []],
          [u'șoseaua',           u'^șoseaua ', []],
          [u'Splaiul',           u'^Splaiul ', []],
          [u'Strada Aleea',      u'^Strada [Aa]lee', []],
          [u'Strada Bulevardul', u'^Strada [Bb]ulevard', []],
          [u'Strada Drumul',     u'^Strada [Dd]rum', []],
          [u'Strada Fundacul',   u'^Strada [Ff]undacul ', []],
          [u'Strada Fundătura',  u'^Strada [Ff]undătura ', []],
          [u'Strada Intrarea',   u'^Strada [Ii]ntrarea ', []],
          [u'Strada Piața',      u'^Strada [Pp]iața ', []],
          [u'Strada Podul',      u'^Strada [Pp]odul ', []],
          [u'Strada Prelungirea',u'^Strada [Pp]relungirea ', []],
          [u'Strada Splaiul',    u'^Strada [Ss]plaiul ', []],
          [u'Strada Strada',     u'^Strada [Ss]trada ', []],
          [u'Strada Ulița',      u'^Strada [Uu]lița ', []],
          [u'Str.',              u'^Str. ', []],
          [u'str.',              u'^str. ', []],
          [u'Strad',             u'^Strad ', []],
          [u'Strda',             u'^Strda ', []],
          [u'Stradela',          u'^Stradela ', []],
          [u'stradela',          u'^stradela ', []],
          [u'str-la',            u'^str-la ', []],
          [u'str.-la',           u'^str\.-la ', []],
          [u'Strada',            u'^Strada ', []],
          [u'strada',            u'^strada ', []],
          [u'Traseu',            u'^Traseu ', []],
          [u'Trecătoarea',       u'^Trecătoarea ', []],
          [u'Trecerea',          u'^Trecerea ', []],
          [u'Ulița',             u'^Ulița ', []],
          [u'Utca',              u' [Uu]tca$', []],
          [u'út',                u' [Úú]t$', []],
          [u'útja',              u' [Úú]tja$', []],
          [u'Улица',             u'^Улица ', []],
          [u'улица',             u'^улица ', []],
          [u'- улица',           u' улица$', []],
          [u'Ул.',               u'^Ул. ', []],
          [u'ул.',               u'^ул. ', []],
          [u'- ул.',             u' ул.$', []],
          [u'ул',                u'^ул', []],
          [u'переулок',          u'^переулок ', []],
          [u'- переулок',        u' переулок$', []],
          [u'пер.',              u'^пер. ', []],
          [u'- пер.',            u' пер.$', []],
          [u'пер',               u'^пер', []],
          [u'туп.',              u'^туп. ', []],
          [u'пр-д',              u'^пр-д ', []],
          [u'пр-т',              u'^пр-т ', []],
          [u'- пр-т',            u' пр-т$', []],
          [u'',                  u'', []] ]

def formatName(input):
  return re.sub(u'[\r\n\t]', ' ', input)

def makeList():
  class NameSearch(object):

    def nodesaddr(self, ways):
      for osmid, tags, refs in ways:
        if u'addr:street' in tags and not u'highway' in tags:
	  formattedName = formatName(tags[u'addr:street'])
	  for streetType, streetRe, streetList in lists:
	    if re.search(streetRe, formattedName):
	      streetList.append(['n', osmid, formattedName])
	      break

    def waysaddr(self, ways):
      for osmid, tags, refs in ways:
        if u'addr:street' in tags and not u'highway' in tags:
	  formattedName = formatName(tags[u'addr:street'])
	  for streetType, streetRe, streetList in lists:
	    if re.search(streetRe, formattedName):
	      streetList.append(['w', osmid, formattedName])
	      break

    def ways(self, ways):
      for osmid, tags, refs in ways:
        if u'name' in tags and u'highway' in tags:
	  formattedName = formatName(tags[u'name'])
	  for streetType, streetRe, streetList in lists:
	    if re.search(streetRe, formattedName):
	      streetList.append(['w', osmid, formattedName])
	      break

    def relations(self, relations):
      for osmid, tags, refs in relations:
        if u'name' in tags and u'type' in tags and tags[u'type'] == 'associatedStreet':
	  formattedName = formatName(tags[u'name'])
	  for streetType, streetRe, streetList in lists:
	    if re.search(streetRe, formattedName):
	      streetList.append(['r', osmid, formattedName])
	      break

  search = NameSearch()
  p = OSMParser(concurrency=4, nodes_callback=None, ways_callback=search.ways, relations_callback=None)
  p.parse('romania.osm')

def writeList():
  d = codecs.open('street-names.txt', 'w', 'utf8')
  for streetType, streetRe, streetList in lists:
    streetList.sort(key=lambda name: name[2])
    first = True
    for item in streetList:
      if first:
        d.write(streetType + '\n')
	first = False
      else:
	d.write(', ')
      d.write(item[0] + str(item[1]))
    d.write('\n')
    for item in streetList:
      d.write(item[2] + '\n')
    d.write('\n\n')
  d.close()

print('Making list.')
makeList()

print('Writing list.')
writeList()

print('Finished.')
