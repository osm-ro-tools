#!/usr/bin/python
# -*- coding: utf-8 -*-
# 
# Requirements: Imposm (Omniscale)
#
# This script searches for highways which contain numbers in their name. Its
# purpose is to find objects which contain the reference number in the name
# tag instead of the ref tag.
#
# © Michael Häckel, GPLv3
#

from imposm.parser import OSMParser
import codecs, re, subprocess

namelist = []

def formatName(input):
  return re.sub(u'[\r\n\t]', ' ', input)

def makeList():
  class NameSearch(object):

    def ways(self, ways):
      for osmid, tags, refs in ways:
        if u'highway' in tags and u'name' in tags \
        and re.search(u'[0-9]', tags[u'name']):
          namelist.append(['w', osmid, formatName(tags[u'name'])])

  search = NameSearch()
  p = OSMParser(concurrency=4, nodes_callback=None, ways_callback=search.ways, relations_callback=None)
  p.parse('romania.osm.pbf')

def writeList():
  d = codecs.open('refs-in-names.txt', 'w', 'utf8')
  namelist.sort(key=lambda name: name[2])
  lastname = ''
  lastnumbers = ''
  for item in namelist:
    if item[2] == lastname:
      lastnumbers = lastnumbers + ', ' + item[0] + str(item[1])
    else:
      if lastnumbers != '':
        d.write(lastnumbers + '\n' + lastname + '\n\n')
      lastname = item[2]
      lastnumbers = item[0] + str(item[1])
  d.write(lastnumbers + '\n' + lastname + '\n\n')
  d.close()

print('Making list.')
makeList()

print('Writing list.')
writeList()

print('Finished.')
